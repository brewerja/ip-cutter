#!/usr/bin/env python

import csv
import os
import re
import sys

from PIL import Image
import pytesseract

INPUT_DIR = '03_cards_png'
HAS_P_BATTING_CARD = re.compile(r'\W*##')
YEAR_TEAM = re.compile(r'(\d{4})\W+(.*)')


def get_card_type(card):
    switch = extract(card, (64, 904, 150, 948))
    if switch == 'G':
        return 'batter'
    if switch == 'W-L':
        return 'pitcher'
    return 'ballpark'


class Card:
    def __init__(self, card, filename):
        self.card = card
        self.filename = filename
        self.set_name()
        self.set_year_and_team()

    def set_name(self):
        name = extract(self.card, (56, 59, 550, 94))
        self.name = HAS_P_BATTING_CARD.split(name)[0].strip().title() \
            .replace('Wle ', 'Kyle ') \
            .replace(' (', '')

    def set_year_and_team(self):
        year_team = extract(self.card, (56, 112, 487, 165))
        match = YEAR_TEAM.search(year_team)
        self.year = int(match.group(1))
        self.team = match.group(2).strip()


def extract(img, box):
    return ocr(img.crop(box))


def ocr(img):
    return pytesseract.image_to_string(img, config='--oem 1 --psm 7')


if __name__ == '__main__':
    with open('04_card_listing.csv', 'w') as f:
        writer = csv.writer(f)
        for filename in sorted(os.listdir(INPUT_DIR)):
            if filename.endswith('.png'):
                card_img = Image.open(os.path.join(INPUT_DIR, filename))

                card_type = get_card_type(card_img)

                c = None
                if card_type in ['batter', 'pitcher']:
                    c = Card(card_img, filename)
                    if c.name != 'Pitchers':
                        print(c.name, card_type, c.filename, c.year, c.team)
                        writer.writerow([c.name, card_type, filename, c.team])
                elif card_type == 'ballpark':
                    pass
                sys.stdout.flush()
