#!/usr/bin/env python

import os
import subprocess

INPUT_DIR = '01_original_pdf'
OUTPUT_DIR = '02_pages_png'
DPI = 300

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

for infile in sorted(os.listdir(INPUT_DIR)):
    if infile.endswith('.pdf'):
        outfile_prefix = infile.split('.')[0]
        subprocess.call(['pdftoppm', '-r', str(DPI), '-png',
                         os.path.join(INPUT_DIR, infile),
                         os.path.join(OUTPUT_DIR, outfile_prefix)])
