#!/usr/bin/env python

import os

from PIL import Image

INPUT_DIR = '02_pages_png'
OUTPUT_DIR = '03_cards_png'

# 300 dpi
LEFT_X, TOP_Y = 30, 60
CARD_W, CARD_H = 827, 1058
LINE_WIDTH = 4

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

for filename in sorted(os.listdir(INPUT_DIR)):
    if filename.endswith('.png'):
        prefix = filename.split('.')[0]
        img = Image.open(os.path.join(INPUT_DIR, filename))
        for i in range(3):
            for j in range(3):
                left_x = LEFT_X + (CARD_W + LINE_WIDTH) * j
                top_y = TOP_Y + (CARD_H + LINE_WIDTH / 2) * i
                card = img.crop((left_x, top_y,
                                 left_x + CARD_W, top_y + CARD_H))
                card_num = 3 * i + j + 1
                card_filename = '%s-%02d.png' % (prefix, card_num)
                output_path = os.path.join(OUTPUT_DIR, card_filename)
                card.save(output_path)
                print(output_path)
